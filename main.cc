// main.cc

// howto compile - FreeBSD: %clang++ -std=c++2a main.cc


#include <iostream>
#include <vector>
#include <cstdlib> // std::srand()
#include <ctime> 
#include <random>
#include <cassert>

//=================[+]
#include <sstream>
#include <fstream> 

namespace disk {
  
  static int next_id_global;
  
  class ProtectedFile {
    
    int &next_id = next_id_global;
    int id;
    std::string name;
    int n = 1;
    bool off{false};
    std::ostringstream os;
    
  public:
    
    auto get_id() const { return id; }
    
    void on() { off = false; }
    
    template <typename T>
    void operator()(const T &s) {
      os << s << std::endl;
    }
    
    template<class T>
    ProtectedFile& operator<< (const T& arg) {
        os << arg;
        return *this;
    }
    
    void operator()(const char mode, const std::string_view &s) {
      if (mode == 'n') {
	      os << n++ << '"' << s << '"' << std::endl;
      }
    }
    ProtectedFile(): id(next_id++) { }
  
    ProtectedFile(const std::string s): id(next_id++), name(s) { }

    ~ProtectedFile() { dump(get_path()); }

    void dump( const std::string &filename) {
      if (off) return;
      std::ofstream ostrm(filename, std::ios::binary);
      ostrm << os.str() << std::endl;
    }
    
    std::string get_path() const {
      if (name.size() == 0) {
        //auto name_ = std::to_string(id);
        auto s = "defname.bin";
        return s;
      }
      auto s = name;
      return s;
    }
    
  };
  
  // ProtectedFile *global_ptr;
}

//=================[-]

namespace datetime {

  void print_current_date() {
    std::time_t t = std::time(nullptr);
    std::tm* now = std::localtime(&t);
 
    std::cout << "Current Date: " << now->tm_mday << '/';
    std::cout << (now->tm_mon + 1) << '/' << (now->tm_year + 1900) << std::endl;
  }
  
}


namespace vector_ex {
  void create(std::vector<std::string> &v, const std::string &filename) {
    std::ifstream istrm(filename, std::ios::binary);
    if (v.empty() && istrm) {
      for (std::string line; getline(istrm, line);) {
        if (line != "" && line.size() > 20) v.push_back(line);
      }
    }
  }
}


namespace cript {
  bool not_is(const std::string &s, char ch) {
    if (s == "") return true;
    for (const auto &c:s) {
      if (c == ch) return false;
    }
    return true;
  }
  size_t master_password_hash = 123456; // start value
  void calc_master_password_hash(std::string &s) {
    for (const auto &c:s) {
      master_password_hash += (size_t)c;
      if (c > 'H') master_password_hash -= 100;
      else master_password_hash += 88;
    }
  }
  size_t calc_hash(const std::string &s) {
    size_t ret = 234567;
    for (const auto &c:s) {
      ret += (size_t)c;
      if (c > 'H') ret -= 100;
      else ret += 88;
    }
    return ret;
  }

  std::string get_tab2(const std::string &t1, size_t hash) {
    std::string ret;
    size_t ha = hash;
    size_t shift = 2;
    while(ret.size() != t1.size()) {
      ha -= shift;
      size_t i = ha & 0x0ff;
      for (const auto &c:t1) {
        
	if (c == (char)i && not_is(ret, (char)i) ) {
	  ret.push_back(c);
	  break;
	}	
      }
      if (ha < 10) {
	ha = hash;
	shift++;
      }
    }
    assert(ret.size() == t1.size());
    return ret;
  }
  // this set of chars not use for new passwords - only for coding:
  std::string replace_table_1 = "1234567890qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM";
  std::string replace_table_2 = "";  
  std::string pack(const std::string &s, const std::string &sold) {
    auto sold_ = cript::calc_hash(sold);
    replace_table_2 = get_tab2(get_tab2(replace_table_1, master_password_hash), sold_);
    std::string ret;
    for (auto &c:s) {
      size_t i = 0;
      char replace_char = c;
      for (auto &char_1:replace_table_1) {
        if (c == char_1) replace_char = replace_table_2[i];
	i++;
      }
      ret.push_back(replace_char);
    }
    return ret;
  }

  std::string unpack(const std::string &s, const std::string &sold) {
    auto sold_ = cript::calc_hash(sold);
    replace_table_2 = get_tab2(get_tab2(replace_table_1, master_password_hash), sold_);
    //replace_table_2 = get_tab2(replace_table_1, master_password_hash);
    std::string ret;
    for (auto &c:s) {
      size_t i = 0;
      char replace_char = c;
      for (auto &char_1:replace_table_2) {
        if (c == char_1) replace_char = replace_table_1[i];
	i++;
      }
      ret.push_back(replace_char);
    }
    return ret;

  }

}


namespace data {
  std::string charset_0 = "0123456789";
  // by default - this set use for generate new passwords:
  std::string charset_1 = \
    "0123456789qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM";
  
  std::vector<std::string>password_manager{\
    "pm 0.1.0 - simply password manager",
    "=> [a]dd [name]  - Create a new password, randomly generated.", 
    "=> [d]el [name]  - Delete a password entry.",
    "=> [e]dit [name] - Edit a password entry with.",
    "=> [l]ist        - List all entries.",
    "=> [s]how [name] - Show password for an entry.",
    "=> [L]ength [length] - Show or set new value. ",
    "=> [r]ead - Read password base from local disk. ",
    "=> [wq] | write-quit - Save password base & exit. ",
    "=> [q]uit - Exit."				\
    };

  struct Pass {
    std::string name;
    std::string password;
    std::string date;
    Pass(std::string s) {
      bool name_f = true;
      bool pass_f = true;
      for (auto &c:s) {
	if (!name_f && !pass_f && c != '*') date.push_back(c);
	if (!name_f && pass_f && c != '*') password.push_back(c);
	if (!name_f && pass_f && c == '*') pass_f = false;
	if (name_f && c != '*') name.push_back(c);
	else {
          name_f = false;
	}
      }
    }
    Pass(const std::string n, const std::string p, const std::string d) {
      name = n; password = p; date = d;
    }
    void unpack() {
      auto s = password;
      password = cript::unpack(s, date);
    }
  };
  std::vector<Pass>passwords{\
    Pass("empty*master key is OK - no save password here*xxxxxxx")};

  int len = 50; // this value only for new passwords
  int legth() { return len; }

    //-----------------------------------[+]
  std::vector<Pass> ReadProtectedFile(const std::string name) {
    std::vector<std::string>text_v;
    vector_ex::create(text_v, name);
    std::vector<Pass>v;
    for (auto &l:text_v) {
      auto p = Pass(l);
      p.unpack();
      v.push_back(p);
    }
    return v;
  }
  //-----------------------------------[-]

}

namespace {
  std::string get_date() {
    std::string s = std::to_string(std::time(nullptr));
    return s;
  }
  std::string enc(data::Pass &p) {
    std::string s;
    s += p.name;
    s += "*";
    s += cript::pack(p.password, p.date);
    s += "*";
    s += p.date;
    return s;
  }
  int solt(int n) { return (n >> 1); }
  int last_random_int = 0;
  int new_rand_int() {
    // use current time as seed for random generator:
    std::srand(last_random_int + std::time(nullptr));
    int random_variable = std::rand();
    std::random_device rd;
    auto seed = rd();
    std::mt19937 gen(seed);
    std::uniform_int_distribution<decltype(seed)> distribute(0, \
		     data::charset_1.size());
    int ret = distribute(gen);
    //int ret = random_variable & 0x0ff;
    if (ret != last_random_int) {
      last_random_int = ret;
      return ret;
    }
    return new_rand_int();
  }
  
  std::string  new_pass(int len) {
    std::string &charset = data::charset_1;
    std::string ret = "";
    int rand;
    for (int i = 0; i <= len; i++) {
      while(true) {
        rand = new_rand_int();
	if (rand >= 0 && rand < charset.size()) break;
      }
      ret.push_back(charset[rand]);
      std::cout << charset[rand];
    }
    std::cout << std::endl;
    return ret;
  }
  std::string get_first(const std::string s) {
    std::string ret = "";
    for (const auto &ch:s) {
      if (ch == ' ' || ch == '=') return ret;
      ret.push_back(ch);
    }
    return "";
  }
  std::string get_2nd(const std::string s) {
    std::string ret = "";
    bool fl = false;
    for (const auto &ch:s) {
      if (fl) ret.push_back(ch);
      if (ch == ' ' || ch == '=') {
	if (!fl) fl = true;
	else return ret;
      }
    }
    return ret;
  }

  int to_int(const std::string s) {
    int ret = 8;
    if (s == "1") return 10;
    if (s == "2") return 20;
    if (s == "3") return 30;    
    if (s == "4") return 40;
    if (s == "5") return 50;
    if (s == "6") return 60;    
    if (s == "7") return 70;
    if (s == "8") return 80;
    if (s == "9") return 90;    

    return ret;
  }
  
  void prn(std::vector<std::string> v) {
    for (const auto &l:v) std::cout << l << std::endl;
  }
  bool main(int arg) {
    if (arg == 1) return true;
    return false;
  }
  bool main(int arg, char **argv) {
    if (arg == 2 && std::string(*(argv + 1)) == "test") {
      prn(data::linux_commons);
      return true;
    }
    if (arg == 2 && std::string(*(argv + 1)) == "pm") {
      prn(data::password_manager);
      std::cout << "enter master key";
      std::cout << ':';
      std::string key;
      getline(std::cin, key);
      cript::calc_master_password_hash(key);
      std::cout << "OK" << std::endl << ':';
      std::string s;
      while(getline(std::cin, s)) {
	if (s == "q" || s == "quit") return true;
	if (s == "l" || s == "list") {
	  for (const auto &p:data::passwords) {
	    std::cout << p.name << ":" << p.date << std::endl;
	  }
	}
	
	if (s == "s" || s == "show") {
	  std::cout << "name:";
	  std::string s;
	  getline(std::cin, s);
	  for (const auto &p:data::passwords) {
	    if (s == p.name) {
	      std::cout << p.password << std::endl;
	    }
	  }
	}
	if (get_first(s) == "s" || get_first(s) == "show") {
	  std::cout << "name:";
	  std::string s_ = get_2nd(s);
	  std::cout << s_ << std::endl;
	  for (const auto &p:data::passwords) {
	    if (s_ == p.name) {
	      std::cout << p.password << " | date =>" << p.date << std::endl;
	    }
	  }
	}
	//-------------------------------[+]
	
	if (s == "a" || s == "add") {
	  std::cout << "name:";
	  std::string name;
	  getline(std::cin, name);
	  auto e = data::Pass(name, new_pass(data::legth()), get_date());
	  data::passwords.push_back(e);
	}
	
	if (get_first(s) == "a" || get_first(s) == "add") {
	  std::string name = get_2nd(s);
          std::cout << "name:" << name << std::endl;
	  auto e = data::Pass(name, new_pass(data::legth()), get_date());
	  data::passwords.push_back(e);
	}


	//-------------------------------[-]
	const std::string def_name = "winpass-default-name.bin";
	if (true) {
          if (s == "wq" || s == "write-quit") {
	    auto f = disk::ProtectedFile(def_name);
            for (auto &e:data::passwords) {
	      f(enc(e));
	    }
	    return true;
	  }
	}
	if (true) {
          if (s == "r" || s == "read") {
	    data::passwords = data::ReadProtectedFile(def_name);
	  }
	}
	
	if (s == "d" || s == "del") {
	  std::cout << "DELETE name:";
	  std::string s_ = "";
	  getline(std::cin, s_);
	  decltype(data::passwords) v = {};
	  for (const auto &p:data::passwords) {
	    if (s_ != p.name) {
	      v.push_back(p);
	    } else std::cout << " deleted " << std::endl;
	  }
	  data::passwords = v;
	}
	if (get_first(s) == "d" || get_first(s) == "del") {
	  std::cout << "DELETE name:";
	  std::string s_ = get_2nd(s);
	  std::cout << s_ << std::endl;
	  decltype(data::passwords) v = {};
	  for (const auto &p:data::passwords) {
	    if (s_ != p.name) {
	      v.push_back(p);
	    } else std::cout << " deleted " << std::endl;
	  }
	  data::passwords = v;
	}
	if (get_first(s) == "L" || get_first(s) == "Length") {
	  std::cout << " old:" << data::len << " new:";
	  data::len = to_int(get_2nd(s));
	  std::cout << data::len << std::endl;
	  
	}
	
	//.......................................................//
	std::cout << ':';
      }
      return true;
    }
    return false;
  }
}

int main(int argc, char **argv, char **envp) {
  if (main(argc)) return EXIT_SUCCESS;
  if (main(argc, argv)) return EXIT_SUCCESS;
  return EXIT_FAILURE;
}

// EOF
